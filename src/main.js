import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import { routes } from './routes';
Vue.config.productionTip = false;
import { store } from './store/store';
import Vuelidate from 'vuelidate'

Vue.use(VueRouter);
Vue.use(Vuelidate)

export const eventBus = new Vue();

const router = new VueRouter({ routes, mode: 'history' });

new Vue({
  router,
  render: h => h(App),
  store,
}).$mount('#app');
