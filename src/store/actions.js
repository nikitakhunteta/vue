import * as types from './types';

//actions to run async code
// dispatch mutations
// can dispatch more than 1 mutations at a time
export default {
    //commit is property of context
    
};


/* lifecycle:
beforeCreate(): before the instance is created
then data and events are initialized
instance created: created()
compiles the template or look from el or mount
beforeMount(): before the template is written to the real DOM
compiled template is mounted to DOM
beforeUpdate(): before DOM is re rendered
updated()
beforeDestroy()
destroyed()
*/