import Vue from 'vue';
import Vuex from 'vuex';
import listing from './modules/listing/listing';
import value from './modules/value/value';

// global ones
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        value: 0
    },
    getters,
    mutations,
    actions,
    modules: {
        listing,
        value
    }
});