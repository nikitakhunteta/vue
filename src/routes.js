import Page1 from './components/Page1.vue';
import Page2 from './components/Page2.vue';
import Page4 from './components/Page4.vue';

const Page3 = resolve => {
  require.ensure(['./components/Page3.vue'], () => {
    resolve(require('./components/Page3.vue'));
  });
};

export const routes = [
  { path: '', component: Page1 },
  { path: '/page1/:id', component: Page1 },
  { path: '/page2', component: Page2 },
  { path: '/page3', component: Page3 },
  { path: '/page4', component: Page4 },
];
